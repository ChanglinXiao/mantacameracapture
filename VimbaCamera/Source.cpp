#include <string>
#include <sstream>
#include <cstring>
#include <iostream>
#include <vector>
#include <algorithm>
#include <windows.h> 
#include "VimbaCPP/Include/VimbaCPP.h"
#include "MantaCamera.h"
#include"StreamSystemInfo.h"
#include"ErrorCodeToMessage.h"
#include"FrameObserver.h"
#include <opencv2/opencv.hpp>
#include "opencv2/highgui/highgui.hpp"

#include <thread>;
#include<direct.h>

using namespace AVT;
using namespace VmbAPI;
using namespace cv;
using namespace std;


const int N_CAM = 2;


Mat img1;
bool img1_good = false;
Mat img2;
bool img2_good = false;
double GetTime()
{
	double dTime = 0.0;
	double m_dFrequency;
#ifdef WIN32
	m_dFrequency = (0.0);
#endif //WIN32
	{
#ifdef WIN32
		LARGE_INTEGER nFrequency;
		QueryPerformanceFrequency(&nFrequency);
		m_dFrequency = (double)nFrequency.QuadPart;
#endif //WIN32
	}
#ifdef WIN32
	LARGE_INTEGER nCounter;
	QueryPerformanceCounter(&nCounter);
	dTime = ((double)nCounter.QuadPart) / m_dFrequency;
#else
	//clock_t nTime = times(NULL);
	//dTime = ((double)(nTime) * 10000.0) / ((double)CLOCKS_PER_SEC);
	struct timespec now;
	clock_gettime(CLOCK_REALTIME, &now);
	dTime = ((double)now.tv_sec) + ((double)now.tv_nsec) / 1000000000.0;
#endif //WIN32

	return dTime;
}
void get_image(CameraPtr m_pCamera, Mat img, String cam_name, int& nu)
{
	nu = 5;
	bool is_initialized = false;
	VmbPixelFormatType ePixelFormat;
	VmbUint32_t nImageSize = 0;
	VmbUint32_t nWidth = 0;
	VmbUint32_t nHeight = 0;
	VmbUchar_t *pImage = NULL;
	VmbFrameStatusType status = VmbFrameStatusIncomplete;
	Mat img_g;

	//////////////
//	while (1)
	{

			AVT::VmbAPI::FramePtr pFrame;
			VmbErrorType    err = m_pCamera->AcquireSingleImage(pFrame, 5000);
			//	m_pCamera->Close();
			if (VmbErrorSuccess == err)
			{
				err = pFrame->GetReceiveStatus(status);
				if (VmbErrorSuccess == err
					&& VmbFrameStatusComplete == status)
				{
					////////process image
					if (!is_initialized)
					{
						//ePixelFormat = VmbPixelFormatMono8;
						err = pFrame->GetPixelFormat(ePixelFormat);
						if (VmbErrorSuccess == err)
						{
							err = pFrame->GetImageSize(nImageSize);
							if (VmbErrorSuccess == err)
							{
								err = pFrame->GetWidth(nWidth);
								if (VmbErrorSuccess == err)
								{
									err = pFrame->GetHeight(nHeight);
									is_initialized = true;
								}
							}
						}
					}
					
					{

						{
							err = pFrame->GetImage(pImage);
							if (VmbErrorSuccess == err)
							{
								if (VmbPixelFormatRgb8 == ePixelFormat)
								{
									img_g = Mat(Size(nWidth, nHeight), CV_8UC3, pImage);
								}
								else
								{
									img_g = Mat(Size(nWidth, nHeight), CV_8UC1, pImage);
								}
								//img = img_g;
								img_g.copyTo(img);
								/*Mat n_img;
								img.copyTo(n_img);
								images.push_back(n_img);*/
								/*imshow("cam_name", *img);
								waitKey(1);*/

							}
						}

					}
				}
			}
		}
}
void get_continue_images(CameraPtr m_pCamera, int cam_id)
{

	FrameObserver*     m_pFrameObserver;
	m_pFrameObserver = new FrameObserver(m_pCamera);
	m_pFrameObserver->camera_id = cam_id;
	vector<CameraPtr> m_pCameras;

	FeaturePtr pFormatFeature;
	// Set pixel format. For the sake of simplicity we only support Mono and BGR in this example.
	VmbErrorType	err = m_pCamera->GetFeatureByName("PixelFormat", pFormatFeature);
	if (VmbErrorSuccess == err)
	{
		// Try to set BGR
		err = pFormatFeature->SetValue(VmbPixelFormatRgb8);
		//res = pFormatFeature->SetValue(VmbPixelFormatBayerGR8);
		if (VmbErrorSuccess != err)
		{
			// Fall back to Mono
			err = pFormatFeature->SetValue(VmbPixelFormatMono8);
		}


		// set camera so that transform algorithms will never fail

		if (VmbErrorSuccess == err)
		{

			err = m_pCamera->StartContinuousImageAcquisition(1, IFrameObserverPtr(m_pFrameObserver));
		}


		if (VmbErrorSuccess == err)
		{
			std::cout << "Press <enter> to stop aquision...\n";
			getchar();
			m_pCamera->StopContinuousImageAcquisition();
		}

	}
}
void sigle_contiue_record()
{
	VimbaSystem&    m_system = VimbaSystem::GetInstance();  // Get a reference to the VimbaSystem singleton
	std::cout << "Vimba Version V" << m_system << "\n";           // Print out version of Vimba
	VmbErrorType    err = m_system.Startup();               // Initialize the Vimba API
	CameraPtrVector cameras;                           // A vector of std::shared_ptr<AVT::VmbAPI::Camera> objects

	std::stringstream strError;

	if (VmbErrorSuccess == err)
	{
		err = m_system.GetCameras(cameras);            // Fetch all cameras known to Vimba
		if (VmbErrorSuccess == err)
		{
			std::cout << "Cameras found: " << cameras.size() << "\n\n";

			// Query all static details of all known cameras and print them out.
			// We don't have to open the cameras for that.
			//	std::for_each(cameras.begin(), cameras.end(), PrintCameraInfo);
		}
	}
	//int n_cam = cameras.size();
		int n_cam = 1;
	vector<FrameObserver*>      m_pFrameObservers;
	for (int i = 0; i < n_cam; i++)
	{
		FrameObserver* m_pFrameObserver = new FrameObserver(cameras[i]);
		m_pFrameObserver->camera_id = i;
		char file_name[100];
		sprintf_s(file_name, "CAM%d", i);
		m_pFrameObserver->file_name = file_name;
		m_pFrameObservers.push_back(m_pFrameObserver);
	}
	//m_pFrameObserver = new FrameObserver(cameras[0]);
	vector<CameraPtr> m_pCameras;
	for (int i = 0; i < n_cam; i++)
	{
		CameraPtr m_pCamera;
		std::string strCameraID;
		cameras[i]->GetID(strCameraID);
		std::cout << "Camera ID:" << strCameraID.c_str() << "\n\n";
		VmbErrorType res = m_system.OpenCameraByID(strCameraID.c_str(), VmbAccessModeFull, m_pCamera);
		m_pCameras.push_back(m_pCamera);

		//if (VmbErrorSuccess == res)
		//{
		//
		//	// Set the GeV packet size to the highest possible value
		//	// (In this example we do not test whether this cam actually is a GigE cam)
		//	FeaturePtr pCommandFeature;
		//	if (VmbErrorSuccess == m_pCamera->GetFeatureByName("GVSPAdjustPacketSize", pCommandFeature))
		//	{
		//		if (VmbErrorSuccess == pCommandFeature->RunCommand())
		//		{
		//			bool bIsCommandDone = false;
		//			do
		//			{
		//				if (VmbErrorSuccess != pCommandFeature->IsCommandDone(bIsCommandDone))
		//				{
		//					break;
		//				}
		//			} while (false == bIsCommandDone);
		//		}
		//	}
		//}
	}
	for (int i = 0; i < n_cam; i++)
	{
		CameraPtr m_pCamera = m_pCameras[i];
		FeaturePtr pFormatFeature;
		// Set pixel format. For the sake of simplicity we only support Mono and BGR in this example.
		err = m_pCamera->GetFeatureByName("PixelFormat", pFormatFeature);
		if (VmbErrorSuccess == err)
		{
			// Try to set BGR
			err = pFormatFeature->SetValue(VmbPixelFormatRgb8);
			//res = pFormatFeature->SetValue(VmbPixelFormatBayerGR8);
			if (VmbErrorSuccess != err)
			{
				// Fall back to Mono
				err = pFormatFeature->SetValue(VmbPixelFormatMono8);
			}
		}
	}
	for (int i = 0; i < n_cam; i++)
	{
		CameraPtr m_pCamera = m_pCameras[i];
		if (VmbErrorSuccess == err)
		{
			// set camera so that transform algorithms will never fail

			if (VmbErrorSuccess == err)
			{

				err = m_pCamera->StartContinuousImageAcquisition(3, IFrameObserverPtr(m_pFrameObservers[i]));
			}
		}
	}
	
	//int a = 1;
	//for (int i = 0; i < n_cam; i++)
	//	a = m_pFrameObservers[i]->image_ready*a;
	bool stop = false;
	long count = 0;
	
	while (!stop)
	{
		double t = (double)getTickCount();
		bool ready=false;
		while (!ready)
		{
			ready = true;
			int a = 1;
			for (int i = 0; i < n_cam; i++)
			{
				a = a*m_pFrameObservers[i]->image_ready*m_pFrameObservers[i]->image_new;
			}
			if (a == 0)
				ready = false;
		}
		for (int i = 0; i < n_cam; i++)
				m_pFrameObservers[i]->image_using = true;

			for (int i = 0; i < n_cam; i++)
			{
				imshow(m_pFrameObservers[i]->file_name, m_pFrameObservers[i]->received_img);
				char c = waitKey(1);
				if (c != -1)
				{
					if ((char)c == 27)	// ESC
					{
						printf("\n");
						stop = true;
					}
				}
			}

			for (int i = 0; i < n_cam; i++)
			{
				m_pFrameObservers[i]->image_using = false;
				m_pFrameObservers[i]->image_new = false;
			}
			count++;
			
			t = ((double)getTickCount() - t) / getTickFrequency();
			cout << "FPS:"<<1 / t << endl;

		}
	if (VmbErrorSuccess == err)
	{
		std::cout << "Press <enter> to stop aquision...\n";
		getchar();
		for (int i = 0; i < n_cam; i++)
		{
			m_pCameras[i]->StopContinuousImageAcquisition();
		}
	}

	//if (VmbErrorSuccess == res)
	//{
	//	// Acquire
	//	res = m_pCamera->AcquireSingleImage(pFrame, 5000);
	//}
	if (VmbErrorSuccess != err)
	{
		// If anything fails after opening the camera we close it
		for (int i = 0; i < n_cam; i++)
		{
			m_pCameras[i]->Close();
		}
	}
	m_system.Shutdown();
	return;
}
void MultipleCameras_SingleImage()
{
	VimbaSystem&    m_system = VimbaSystem::GetInstance();  // Get a reference to the VimbaSystem singleton
	std::cout << "Vimba Version V" << m_system << "\n";           // Print out version of Vimba
	VmbErrorType    err = m_system.Startup();               // Initialize the Vimba API
	CameraPtrVector cameras;                           // A vector of std::shared_ptr<AVT::VmbAPI::Camera> objects

	std::stringstream strError;

	if (VmbErrorSuccess == err)
	{
		err = m_system.GetCameras(cameras);            // Fetch all cameras known to Vimba
		if (VmbErrorSuccess == err)
		{
			std::cout << "Cameras found: " << cameras.size() << "\n\n";

			// Query all static details of all known cameras and print them out.
			// We don't have to open the cameras for that.
			//	std::for_each(cameras.begin(), cameras.end(), PrintCameraInfo);
		}
	}
	int n_cam = cameras.size();
	//int n_cam = 2;
	//	FramePtrVector frames(3); // A list of frames for streaming. We chose
	// to queue 3 frames.
	//	FrameObserver pObserver(cameras[0]); // Our implementation
	// of a frame observer
	vector<FrameObserver*>      m_pFrameObservers;
	for (int i = 0; i < n_cam; i++)
	{
		FrameObserver* m_pFrameObserver = new FrameObserver(cameras[i]);
		m_pFrameObservers.push_back(m_pFrameObserver);
	}
	//m_pFrameObserver = new FrameObserver(cameras[0]);
	vector<CameraPtr> m_pCameras;
	for (int i = 0; i < n_cam; i++)
	{
		CameraPtr m_pCamera;
		std::string strCameraID;
		cameras[i]->GetID(strCameraID);
		std::cout << "Camera ID:" << strCameraID.c_str() << "\n\n";
		VmbErrorType res = m_system.OpenCameraByID(strCameraID.c_str(), VmbAccessModeFull, m_pCamera);


		if (VmbErrorSuccess == res)
		{
			m_pCameras.push_back(m_pCamera);
			// Set the GeV packet size to the highest possible value
			// (In this example we do not test whether this cam actually is a GigE cam)
			FeaturePtr pCommandFeature;
			if (VmbErrorSuccess == m_pCamera->GetFeatureByName("GVSPAdjustPacketSize", pCommandFeature))
			{
				if (VmbErrorSuccess == pCommandFeature->RunCommand())
				{
					bool bIsCommandDone = false;
					do
					{
						if (VmbErrorSuccess != pCommandFeature->IsCommandDone(bIsCommandDone))
						{
							break;
						}
					} while (false == bIsCommandDone);
				}
			}
		}
	}
	for (int i = 0; i < n_cam; i++)
	{
		CameraPtr m_pCamera = m_pCameras[i];
		FeaturePtr pFormatFeature;
		// Set pixel format. For the sake of simplicity we only support Mono and BGR in this example.
		err = m_pCamera->GetFeatureByName("PixelFormat", pFormatFeature);
		if (VmbErrorSuccess == err)
		{
			// Try to set BGR
			err = pFormatFeature->SetValue(VmbPixelFormatRgb8);
			//res = pFormatFeature->SetValue(VmbPixelFormatBayerGR8);
			if (VmbErrorSuccess != err)
			{
				// Fall back to Mono
				err = pFormatFeature->SetValue(VmbPixelFormatMono8);
			}
		}
	}
	long count = 0;
	double last_received_time = 0;

	///////////////
	char file_name[100];
	vector<string> window_names;
	for (int i = 0; i < n_cam; i++)
	{
		sprintf_s(file_name, "C:/CAM%d/", i);

		_mkdir(file_name);//

		sprintf_s(file_name, "CAM%d", i);
		namedWindow(file_name);

		window_names.push_back(file_name);
	}
	//////////////////////

	bool is_initialized = false;
	VmbPixelFormatType ePixelFormat;
	VmbUint32_t nImageSize = 0;
	VmbUint32_t nWidth = 0;
	VmbUint32_t nHeight = 0;
	VmbUchar_t *pImage = NULL;
	VmbFrameStatusType status = VmbFrameStatusIncomplete;
	Mat img;
	vector<Mat> images(n_cam);
	//////////////
	while (1)
	{
		
		for (int i = 0; i < n_cam; i++)
		{
			
			CameraPtr m_pCamera = m_pCameras[i];
			//	if (VmbErrorSuccess == err)

			//				std::cout << "Camera ID:" << m_pCamera->GetID.c_str() << "\n\n";

			AVT::VmbAPI::FramePtr pFrame;
			err = m_pCamera->AcquireSingleImage(pFrame, 5000);
			//	m_pCamera->Close();
			if (VmbErrorSuccess == err)
			{
				err = pFrame->GetReceiveStatus(status);
				if (VmbErrorSuccess == err
					&& VmbFrameStatusComplete == status)
				{
					////////process image
					if (!is_initialized)
					{
						//ePixelFormat = VmbPixelFormatMono8;
						err = pFrame->GetPixelFormat(ePixelFormat);
						if (VmbErrorSuccess == err)
						{
							err = pFrame->GetImageSize(nImageSize);
							if (VmbErrorSuccess == err)
							{
								err = pFrame->GetWidth(nWidth);
								if (VmbErrorSuccess == err)
								{
									err = pFrame->GetHeight(nHeight);
									is_initialized = true;
								}
							}
						}
					}
					
					{

						{
							err = pFrame->GetImage(pImage);
							if (VmbErrorSuccess == err)
							{
								if (VmbPixelFormatRgb8 == ePixelFormat)
								{
									img = Mat(Size(nWidth, nHeight), CV_8UC3, pImage);
								}
								else
								{
									img = Mat(Size(nWidth, nHeight), CV_8UC1, pImage);
								}
								Mat n_img;
								img.copyTo(n_img);
								images[i]=n_img;
								//imshow("img", images[i]);
								//waitKey(1);

							}
						}

					}
				}
			}
		}
		//imshow("img", images[0]);
		//waitKey(1);
		bool stop = false;
		if (images.size() == n_cam)
		{
			for (int i = 0; i < n_cam; i++)
			{
				char file_name[100];
				sprintf_s(file_name, "C:/CAM%d/image_%04d.jpg", i, count);
				if (images[i].empty())
					break;
			
				/*imshow("img", images[i]);
				waitKey(1);*/
imshow(window_names[i], images[i]);
				char c = waitKey(1);
				if (c != -1)
				{
					if ((char)c == 27)	// ESC
					{
						printf("\n");
						stop = true;
					}
				}
				//	imwrite(String(file_name), images[i]);
			}

				double received_time = GetTime();
				if (last_received_time == 0)
					last_received_time = received_time;
				else
				{
					double fps = 1.0 / (received_time - last_received_time);
					std::cout << count << "  FPS:" << fps << std::endl;
					last_received_time = received_time;
				}
				
			
			count++;
		}
		if (stop)
			break;
		////if (VmbErrorSuccess == err)
		//{
		//	std::cout << "Press <enter> to stop aquision...\n";
		//	getchar();
		//	break;
		//}

	}
	//if (VmbErrorSuccess == res)
	//{
	//	// Acquire
	//	res = m_pCamera->AcquireSingleImage(pFrame, 5000);
	//}
	//if (VmbErrorSuccess != err)

	// If anything fails after opening the camera we close it
	for (int i = 0; i < n_cam; i++)
	{
		m_pCameras[i]->Close();
	}

	m_system.Shutdown();

}
void MultipleCameras_SingleImage_thred()
{
	VimbaSystem&    m_system = VimbaSystem::GetInstance();  // Get a reference to the VimbaSystem singleton
	std::cout << "Vimba Version V" << m_system << "\n";           // Print out version of Vimba
	VmbErrorType    err = m_system.Startup();               // Initialize the Vimba API
	CameraPtrVector cameras;                           // A vector of std::shared_ptr<AVT::VmbAPI::Camera> objects

	std::stringstream strError;

	if (VmbErrorSuccess == err)
	{
		err = m_system.GetCameras(cameras);            // Fetch all cameras known to Vimba
		if (VmbErrorSuccess == err)
		{
			std::cout << "Cameras found: " << cameras.size() << "\n\n";

			// Query all static details of all known cameras and print them out.
			// We don't have to open the cameras for that.
			//	std::for_each(cameras.begin(), cameras.end(), PrintCameraInfo);
		}
	}
	int n_cam = cameras.size();
	//int n_cam = 2;
	//	FramePtrVector frames(3); // A list of frames for streaming. We chose
	// to queue 3 frames.
	//	FrameObserver pObserver(cameras[0]); // Our implementation
	// of a frame observer
	vector<FrameObserver*>      m_pFrameObservers;
	for (int i = 0; i < n_cam; i++)
	{
		FrameObserver* m_pFrameObserver = new FrameObserver(cameras[i]);
		m_pFrameObservers.push_back(m_pFrameObserver);
	}
	//m_pFrameObserver = new FrameObserver(cameras[0]);
	vector<CameraPtr> m_pCameras;
	for (int i = 0; i < n_cam; i++)
	{
		CameraPtr m_pCamera;
		std::string strCameraID;
		cameras[i]->GetID(strCameraID);
		std::cout << "Camera ID:" << strCameraID.c_str() << "\n\n";
		VmbErrorType res = m_system.OpenCameraByID(strCameraID.c_str(), VmbAccessModeFull, m_pCamera);


		if (VmbErrorSuccess == res)
		{
			m_pCameras.push_back(m_pCamera);
			// Set the GeV packet size to the highest possible value
			// (In this example we do not test whether this cam actually is a GigE cam)
			FeaturePtr pCommandFeature;
			if (VmbErrorSuccess == m_pCamera->GetFeatureByName("GVSPAdjustPacketSize", pCommandFeature))
			{
				if (VmbErrorSuccess == pCommandFeature->RunCommand())
				{
					bool bIsCommandDone = false;
					do
					{
						if (VmbErrorSuccess != pCommandFeature->IsCommandDone(bIsCommandDone))
						{
							break;
						}
					} while (false == bIsCommandDone);
				}
			}
		}
	}
	for (int i = 0; i < n_cam; i++)
	{
		CameraPtr m_pCamera = m_pCameras[i];
		FeaturePtr pFormatFeature;
		// Set pixel format. For the sake of simplicity we only support Mono and BGR in this example.
		err = m_pCamera->GetFeatureByName("PixelFormat", pFormatFeature);
		if (VmbErrorSuccess == err)
		{
			// Try to set BGR
			err = pFormatFeature->SetValue(VmbPixelFormatRgb8);
			//res = pFormatFeature->SetValue(VmbPixelFormatBayerGR8);
			if (VmbErrorSuccess != err)
			{
				// Fall back to Mono
				err = pFormatFeature->SetValue(VmbPixelFormatMono8);
			}
		}
	}
	long count = 0;
	double last_received_time = 0;

	///////////////
	char file_name[100];
	vector<string> window_names;
	for (int i = 0; i < n_cam; i++)
	{
		sprintf_s(file_name, "C:/CAM%d/", i);

		_mkdir(file_name);//

		sprintf_s(file_name, "CAM%d", i);
		namedWindow(file_name);

		window_names.push_back(file_name);
	}
	//////////////////////

	bool is_initialized = false;
	VmbPixelFormatType ePixelFormat;
	VmbUint32_t nImageSize = 0;
	VmbUint32_t nWidth = 0;
	VmbUint32_t nHeight = 0;
	VmbUchar_t *pImage = NULL;
	VmbFrameStatusType status = VmbFrameStatusIncomplete;
	Mat img;
	vector<Mat> images(2);
	//////////////
	while (1)
	{
		Mat img1;
		Mat img2;
		int s1;
		int s2;
		thread t1(get_image, m_pCameras[0], img1, "cam1",s1);
		thread t2(get_image, m_pCameras[1], img2, "cam2",s2);
		t1.join();
		t2.join();
		
		Mat img1_1 = img1;
		Mat img2_2 = img2;
		images.push_back(img1_1);
		images.push_back(img2_2);
		//imshow("return_img", images[0]);
		//waitKey(1);



		bool stop = false;
		if (images.size() == n_cam)
		{
			for (int i = 0; i < n_cam; i++)
			{
				char file_name[100];
				sprintf_s(file_name, "C:/CAM%d/image_%04d.jpg", i, count);
				//	if (images[i].empty())
				//		continue;
				/*imshow("img", images[i]);
				waitKey(1);*/

				//	imwrite(String(file_name), images[i]);

			}
				double received_time = GetTime();
				if (last_received_time == 0)
					last_received_time = received_time;
				else
				{
					double fps = 1.0 / (received_time - last_received_time);
					std::cout << count << "  FPS:" << fps << std::endl;
					last_received_time = received_time;
				}
			//	imshow(window_names[i], images[i]);
				char c = waitKey(1);
				if (c != -1)
				{
					if ((char)c == 27)	// ESC
					{
						printf("\n");
						stop = true;
					}
				}
			
			count++;
		}
		if (stop)
			break;
	}
	// If anything fails after opening the camera we close it
	for (int i = 0; i < n_cam; i++)
	{
		m_pCameras[i]->Close();
	}

	m_system.Shutdown();

}
//void MultipleCameras_contiueImage_thred()
//{
//	VimbaSystem&    m_system = VimbaSystem::GetInstance();  // Get a reference to the VimbaSystem singleton
//	std::cout << "Vimba Version V" << m_system << "\n";           // Print out version of Vimba
//	VmbErrorType    err = m_system.Startup();               // Initialize the Vimba API
//	CameraPtrVector cameras;                           // A vector of std::shared_ptr<AVT::VmbAPI::Camera> objects
//
//	std::stringstream strError;
//
//	if (VmbErrorSuccess == err)
//	{
//		err = m_system.GetCameras(cameras);            // Fetch all cameras known to Vimba
//		if (VmbErrorSuccess == err)
//		{
//			std::cout << "Cameras found: " << cameras.size() << "\n\n";
//
//			// Query all static details of all known cameras and print them out.
//			// We don't have to open the cameras for that.
//			//	std::for_each(cameras.begin(), cameras.end(), PrintCameraInfo);
//		}
//	}
//	int n_cam = cameras.size();
//	//int n_cam = 2;
//	//	FramePtrVector frames(3); // A list of frames for streaming. We chose
//	// to queue 3 frames.
//	//	FrameObserver pObserver(cameras[0]); // Our implementation
//	// of a frame observer
//
//	for (int i = 0; i < n_cam; i++)
//	{
//		CameraPtr m_pCamera;
//		std::string strCameraID;
//		cameras[i]->GetID(strCameraID);
//		std::cout << "Camera ID:" << strCameraID.c_str() << "\n\n";
//		VmbErrorType res = m_system.OpenCameraByID(strCameraID.c_str(), VmbAccessModeFull, m_pCamera);
//
//
//		if (VmbErrorSuccess == res)
//		{
//			// Set the GeV packet size to the highest possible value
//			// (In this example we do not test whether this cam actually is a GigE cam)
//			FeaturePtr pCommandFeature;
//			if (VmbErrorSuccess == m_pCamera->GetFeatureByName("GVSPAdjustPacketSize", pCommandFeature))
//			{
//				if (VmbErrorSuccess == pCommandFeature->RunCommand())
//				{
//					bool bIsCommandDone = false;
//					do
//					{
//						if (VmbErrorSuccess != pCommandFeature->IsCommandDone(bIsCommandDone))
//						{
//							break;
//						}
//					} while (false == bIsCommandDone);
//				}
//			}
//		}
//	}
//
////	for (int i = 0; i < n_cam; i++)
//	{
//		thread t1(get_continue_images, 0);
//		thread t2(get_continue_images, 1);
//		while 
//		{
//			if (cam0_ready&cam1_ready)
//			{
//				//calculate position
//				//send;
//			}
//		}
//		CameraPtr m_pCamera = m_pCameras[i];
//		FeaturePtr pFormatFeature;
//		// Set pixel format. For the sake of simplicity we only support Mono and BGR in this example.
//		err = m_pCamera->GetFeatureByName("PixelFormat", pFormatFeature);
//		if (VmbErrorSuccess == err)
//		{
//			// Try to set BGR
//			err = pFormatFeature->SetValue(VmbPixelFormatRgb8);
//			//res = pFormatFeature->SetValue(VmbPixelFormatBayerGR8);
//			if (VmbErrorSuccess != err)
//			{
//				// Fall back to Mono
//				err = pFormatFeature->SetValue(VmbPixelFormatMono8);
//			}
//		}
//	}
//	long count = 0;
//	double last_received_time = 0;
//
//	///////////////
//	char file_name[100];
//	vector<string> window_names;
//	for (int i = 0; i < n_cam; i++)
//	{
//		sprintf_s(file_name, "C:/CAM%d/", i);
//
//		_mkdir(file_name);//
//
//		sprintf_s(file_name, "CAM%d", i);
//		namedWindow(file_name);
//
//		window_names.push_back(file_name);
//	}
//	//////////////////////
//
//	bool is_initialized = false;
//	VmbPixelFormatType ePixelFormat;
//	VmbUint32_t nImageSize = 0;
//	VmbUint32_t nWidth = 0;
//	VmbUint32_t nHeight = 0;
//	VmbUchar_t *pImage = NULL;
//	VmbFrameStatusType status = VmbFrameStatusIncomplete;
//	Mat img;
//	vector<Mat> images(2);
//	//////////////
//	while (1)
//	{
//		Mat img1;
//		Mat img2;
//		int s1;
//		int s2;
//		thread t1(get_image, m_pCameras[0], img1, "cam1", s1);
//		thread t2(get_image, m_pCameras[1], img2, "cam2", s2);
//		t1.join();
//		t2.join();
//
//		Mat img1_1 = img1;
//		Mat img2_2 = img2;
//		images.push_back(img1_1);
//		images.push_back(img2_2);
//		//imshow("return_img", images[0]);
//		//waitKey(1);
//
//
//
//		bool stop = false;
//		if (images.size() == n_cam)
//		{
//			for (int i = 0; i < n_cam; i++)
//			{
//				char file_name[100];
//				sprintf_s(file_name, "C:/CAM%d/image_%04d.jpg", i, count);
//				//	if (images[i].empty())
//				//		continue;
//				/*imshow("img", images[i]);
//				waitKey(1);*/
//
//				//	imwrite(String(file_name), images[i]);
//
//			}
//			double received_time = GetTime();
//			if (last_received_time == 0)
//				last_received_time = received_time;
//			else
//			{
//				double fps = 1.0 / (received_time - last_received_time);
//				std::cout << count << "  FPS:" << fps << std::endl;
//				last_received_time = received_time;
//			}
//			//	imshow(window_names[i], images[i]);
//			char c = waitKey(1);
//			if (c != -1)
//			{
//				if ((char)c == 27)	// ESC
//				{
//					printf("\n");
//					stop = true;
//				}
//			}
//
//			count++;
//		}
//		if (stop)
//			break;
//	}
//	// If anything fails after opening the camera we close it
//	for (int i = 0; i < n_cam; i++)
//	{
//		m_pCameras[i]->Close();
//	}
//
//	m_system.Shutdown();
//
//}
int main(int argc, char* argv[])
{
	sigle_contiue_record();
}
void backup()
{
	int numCameras = 0;
	VimbaSystem&    m_system = VimbaSystem::GetInstance();  // Get a reference to the VimbaSystem singleton
	std::cout << "Vimba Version V" << m_system << "\n";           // Print out version of Vimba
	VmbErrorType    err = m_system.Startup();               // Initialize the Vimba API
	CameraPtrVector cameras;                           // A vector of std::shared_ptr<AVT::VmbAPI::Camera> objects
	int i;
	std::stringstream strError;

	if (VmbErrorSuccess == err)
	{
		err = m_system.GetCameras(cameras);            // Fetch all cameras known to Vimba
		if (VmbErrorSuccess == err)
		{
			std::cout << "Cameras found: " << cameras.size() << "\n\n";

			// Query all static details of all known cameras and print them out.
			// We don't have to open the cameras for that.
			//	std::for_each(cameras.begin(), cameras.end(), PrintCameraInfo);
		}
		numCameras = cameras.size();
	}
	for (i = 0; i < numCameras; i++)
	{
		//PvCaptureAdjustPacketSize(Camera[i].Handle, 8228);
		//PvCaptureStart(Camera[i].Handle);
		//PvCaptureQueueFrame(Camera[i].Handle, &(Camera[i].Frame), NULL);
		//PvAttrUint32Set(Camera[i].Handle, "StreamBytesPerSecond", StreamBytesPerSecondPerCamera);
		///*PvAttrEnumSet(Camera[i].Handle, "PixelFormat", "Mono8");*/
		//PvAttrEnumSet(Camera[i].Handle, "PixelFormat", "BGR8");
		////PvAttrEnumSet(Camera[i].Handle, "PixelFormat", "BayerRG8");
		////	PvAttrEnumSet(Camera[i].Handle, "ExposureMode", "Auto");	
		//PvAttrEnumSet(Camera[i].Handle, "ExposureMode", "Manual");
		//PvAttrUint32Set(Camera[i].Handle, "ExposureValue", ExposureValue);
		//PvAttrEnumSet(Camera[i].Handle, "GainMode", "Auto");
		////	PvAttrEnumSet(Camera[i].Handle, "GainMode", "AutoOnce");
		//PvAttrEnumSet(Camera[i].Handle, "FrameStartTriggerMode", "Software");
		//PvAttrEnumSet(Camera[i].Handle, "AcquisitionMode", "Continuous");
		//PvCommandRun(Camera[i].Handle, "AcquisitionStart");
	}
}
//void ListCameras()
//{
//	VimbaSystem&    sys = VimbaSystem::GetInstance();  // Get a reference to the VimbaSystem singleton
//	std::cout << "Vimba Version V" << sys << "\n";           // Print out version of Vimba
//	VmbErrorType    err = sys.Startup();               // Initialize the Vimba API
//	CameraPtrVector cameras;                           // A vector of std::shared_ptr<AVT::VmbAPI::Camera> objects
//
//	std::stringstream strError;
//
//	if (VmbErrorSuccess == err)
//	{
//		err = sys.GetCameras(cameras);            // Fetch all cameras known to Vimba
//		if (VmbErrorSuccess == err)
//		{
//			std::cout << "Cameras found: " << cameras.size() << "\n\n";
//
//			// Query all static details of all known cameras and print them out.
//			// We don't have to open the cameras for that.
//		//	std::for_each(cameras.begin(), cameras.end(), PrintCameraInfo);
//		}
//		else
//		{
//		//	std::cout << "Could not list cameras. Error code: " << err << "(" <<( ErrorCodeToMessage(err)) << ")" << "\n";
//		}
//
//		sys.Shutdown();                             // Close Vimba
//	}
//	else
//	{
//	//	std::cout << "Could not start system. Error code: " << err << "(" << ErrorCodeToMessage(err) << ")" << "\n";
//	}
//}
//void PrintCameraInfo(const CameraPtr &camera)
//{
//	std::string strID;
//	std::string strName;
//	std::string strModelName;
//	std::string strSerialNumber;
//	std::string strInterfaceID;
//
//	std::ostringstream ErrorStream;
//
//	VmbErrorType err = camera->GetID(strID);
//	if (VmbErrorSuccess != err)
//	{
//		ErrorStream << "[Could not get camera ID. Error code: " << err << "(" << AVT::VmbAPI::ErrorCodeToMessage(err) << ")" << "]";
//		strID = ErrorStream.str();
//	}
//
//	err = camera->GetName(strName);
//	if (VmbErrorSuccess != err)
//	{
//		ErrorStream << "[Could not get camera name. Error code: " << err << "(" << AVT::VmbAPI::ErrorCodeToMessage(err) << ")" << "]";
//		strName = ErrorStream.str();
//	}
//
//	err = camera->GetModel(strModelName);
//	if (VmbErrorSuccess != err)
//	{
//		ErrorStream << "[Could not get camera mode name. Error code: " << err << "(" << AVT::VmbAPI::ErrorCodeToMessage(err) << ")" << "]";
//		strModelName = ErrorStream.str();
//	}
//
//	err = camera->GetSerialNumber(strSerialNumber);
//	if (VmbErrorSuccess != err)
//	{
//		ErrorStream << "[Could not get camera serial number. Error code: " << err << "(" << AVT::VmbAPI::ErrorCodeToMessage(err) << ")" << "]";
//		strSerialNumber = ErrorStream.str();
//	}
//
//	err = camera->GetInterfaceID(strInterfaceID);
//	if (VmbErrorSuccess != err)
//	{
//		ErrorStream << "[Could not get interface ID. Error code: " << err << "(" << AVT::VmbAPI::ErrorCodeToMessage(err) << ")" << "]";
//		strInterfaceID = ErrorStream.str();
//	}
//
//	std::cout << "/// Camera Name    : " << strName << "\n"
//		<< "/// Model Name     : " << strModelName << "\n"
//		<< "/// Camera ID      : " << strID << "\n"
//		<< "/// Serial Number  : " << strSerialNumber << "\n"
//		<< "/// @ Interface ID : " << strInterfaceID << "\n\n";
//}